
<?php
// chemin d'accès à votre fichier JSON
$file = 'DataBase.json'; 
// mettre le contenu du fichier dans une variable
$data = file_get_contents($file); 
// décoder le flux JSON
$obj = json_decode($data);
echo "<pre>";
//var_dump ( $obj -> StyleMusique[0]);
//var_dump($obj);
echo "</pre>";

?>

<!DOCTYPE html>
<html lang="fr">

  <head>
    <meta charset="utf-8">
    <meta name="description" content="page HTML5 ">
    <meta name="JulienPeltot" content="RechercheMusique">

    <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.1/build/pure-min.css" integrity="sha384-oAOxQR6DkCoMliIh8yFnu25d7Eq/PHS21PClpwjOTeU2jRSq11vu66rf90/cZr47" crossorigin="anonymous">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="App.js"> </script>

    <title>Votre Musique !</title>
  </head>

  <body>

      <header>
            <h1>Un style de musique ?</h1>
      </header>

      <main>
        <label for="choix_musique">Indiquez votre style de musique préférée :</label>
        <input list="musique" type="text" id="choix_musique">
            <datalist id="musique">
                <option value=' <?php echo  $obj -> StyleMusique[0];  ?>'>
            </datalist>      
        </input>   
      </main>

      <footer>
          <p>Made by Julien Peltot</p>
      </footer>
    
  </body>

</html>